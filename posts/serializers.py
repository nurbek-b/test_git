from rest_framework import serializers
from .models import Post, Comment


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'author', 'title', 'body', 'created_at')
        model = Post


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'author', 'post', 'body', 'created_date')
        model = Comment


class CommentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('post', 'author', 'body')
