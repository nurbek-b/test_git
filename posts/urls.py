from django.urls import path
from .views import *


urlpatterns = [
    path('<int:pk>/', PostDetail.as_view(), name='detail'),
    path('', PostList.as_view(), name='list'),
    path('comment/', CommentList.as_view(), name='comment_list'),
    path('comment/create/', CommentCreateView.as_view(), name='comment_create'),
    # path('create/', PostCreate.as_view()),
    # path('<int:pk>/update/', PostUpdate.as_view()),
    # path('<int:pk>/delete/', PostDelete.as_view()),
]