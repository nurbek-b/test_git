from rest_framework import generics, permissions, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer, CommentCreateSerializer
from .permissions import IsAuthorOrReadOnly
from .pegination import ListPagination
from django_filters import rest_framework as filterset
from rest_framework import filters


class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    pagination_class = ListPagination
    filter_backends = (filterset.DjangoFilterBackend, filters.SearchFilter)
    filterset_fields = ('author', 'title', 'created_at')
    search_fields = ('author', 'title', 'created_at')


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CommentList(generics.ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    pagination_class = ListPagination


class CommentCreateView(generics.CreateAPIView):
    serializer_class = CommentCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        post = Post.objects.get(pk=request.data['post'])
        if post.author == self.request.user:
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response({'Comment created successfully'}, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({'You do not have permissions'}, status=status.HTTP_400_BAD_REQUEST)

#
# class PostDelete(generics.DestroyAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#
# class PostCreate(generics.CreateAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#
# class PostUpdate(generics.UpdateAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
