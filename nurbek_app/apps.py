from django.apps import AppConfig


class NurbekAppConfig(AppConfig):
    name = 'nurbek_app'
